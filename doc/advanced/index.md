# Advanced Configuration

- Using an [external PostgreSQL](external-db/index.md)
- Using an [external Redis](external-redis/index.md)
- Using an [external Gitaly](external-gitaly/index.md)
- Using your own [nginx ingress controller](external-nginx/index.md)
- After install, [managing Persistent Volumes](persistent-volumes/index.md)
